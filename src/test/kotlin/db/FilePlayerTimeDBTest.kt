package db

import com.google.gson.Gson
import java.io.File
import java.util.UUID
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotNull
import models.Player
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Nested

class FilePlayerTimeDBTest {

    private var dbDir = createTempDir()

    @BeforeTest
    fun setUp() {
        dbDir = createTempDir()
    }

    @AfterTest
    fun cleanUp() {
        dbDir.delete()
    }

    @Test
    fun `find user over uuid file name`() {
        val uuid = UUID.randomUUID()
        val fileDB = FilePlayerTimeDB(dbDir)

        assertFalse(fileDB.existsById(uuid))

        File(dbDir, uuid.toString()).createNewFile()
        assertTrue(fileDB.existsById(uuid))
    }

    @Test
    fun `create new player file`() {
        val uuid = UUID.randomUUID()
        val fileDB = FilePlayerTimeDB(dbDir)
        val player = Player(uuid, "username")

        assertFalse(fileDB.existsById(uuid))

        fileDB.create(player)

        assertTrue(fileDB.existsById(uuid))

        val writtenFile = File(dbDir, uuid.toString())
        assertEquals(player, Gson().fromJson(writtenFile.readText(), Player::class.java))
    }

    @Nested
    inner class ReadPlayer {

        @Test
        fun `read player from file`() {
            val uuid = UUID.randomUUID()
            val fileDB = FilePlayerTimeDB(dbDir)
            val player = Player(uuid, "username")

            File(dbDir, uuid.toString()).writeText(Gson().toJson(player))

            assertEquals(player, fileDB.findById(uuid))
        }

        @Test
        fun `read player from file through player name`() {
            val playerName = "playerName"
            val fileDB = FilePlayerTimeDB(dbDir)
            val uuid = UUID.randomUUID()
            val player = Player(uuid, playerName)

            val file = File(dbDir, uuid.toString())
            assertTrue(file.createNewFile())

            file.writeText(Gson().toJson(player))

            val readPlayer = fileDB.findByName(playerName)
            assertNotNull(readPlayer)
            assertEquals(player, readPlayer)
        }

        @Test
        fun `read not existing player from file through player name`() {
            val fileDB = FilePlayerTimeDB(dbDir)

            val playerName = "foo"
            val exception = assertFailsWith<PlayerNotFoundException> { fileDB.findByName(playerName) }
            assertEquals("""The player with the name $playerName was not found""", exception.message)
        }

        @Test
        fun `read not existing player from file through player id`() {
            val fileDB = FilePlayerTimeDB(dbDir)

            val uuid = UUID.randomUUID()
            val exception = assertFailsWith<PlayerNotFoundException> { fileDB.findById(uuid) }
            assertEquals("""The player with the id $uuid was not found""", exception.message)
        }
    }

    @Test
    fun `write player to file`() {
        val uuid = UUID.randomUUID()
        val fileDB = FilePlayerTimeDB(dbDir)
        val player = Player(uuid, "username")

        val file = File(dbDir, uuid.toString())
        assertTrue(file.createNewFile())
        fileDB.save(player)
        assertEquals(player, Gson().fromJson(file.readText(), Player::class.java))
    }

    @Nested
    inner class FindPlayerByName {

        @Test
        fun `find player over playerName`() {
            val playerName = "playerName"
            val fileDB = FilePlayerTimeDB(dbDir)
            val uuid = UUID.randomUUID()
            val player = Player(uuid, playerName)

            val file = File(dbDir, uuid.toString())
            assertTrue(file.createNewFile())

            file.writeText(Gson().toJson(player))
            assertTrue(fileDB.existsByName(playerName))
        }

        @Test
        fun `return false if there is no file`() {
            val playerName = "playerName"
            val fileDB = FilePlayerTimeDB(dbDir)

            assertFalse(fileDB.existsByName(playerName))
        }

        @Test
        fun `return false if there is no file matching the name`() {
            val playerName = "playerName"
            val fileDB = FilePlayerTimeDB(dbDir)
            val uuid = UUID.randomUUID()
            val player = Player(uuid, playerName)

            val file = File(dbDir, uuid.toString())
            assertTrue(file.createNewFile())

            file.writeText(Gson().toJson(player))
            assertFalse(fileDB.existsByName("foo"))
        }
    }

    @Test
    fun `find all players`() {
        val playerName = "playerName"
        val fileDB = FilePlayerTimeDB(dbDir)
        val uuid = UUID.randomUUID()
        val uuid2 = UUID.randomUUID()
        val player = Player(uuid, playerName)
        val player2 = Player(uuid2, playerName)

        File(dbDir, uuid.toString()).writeText(Gson().toJson(player))
        File(dbDir, uuid2.toString()).writeText(Gson().toJson(player2))

        val playerList = fileDB.findAll()
        assertFalse(playerList.isEmpty())
        assertEquals(2, playerList.size)
        assertTrue(playerList.contains(player))
        assertTrue(playerList.contains(player2))
    }

    @Test
    fun `find all players if there is no one`() {
        val fileDB = FilePlayerTimeDB(dbDir)

        assertTrue(fileDB.findAll().isEmpty())
    }

    @Test
    fun `error by deserialization`() {
        val fileDB = FilePlayerTimeDB(dbDir)
        val uuid = UUID.randomUUID()

        File(dbDir, uuid.toString()).writeText("Foo")

        assertTrue(fileDB.findAll().isEmpty())
    }
}
