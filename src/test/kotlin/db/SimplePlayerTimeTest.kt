package db

import SimplePlayerTime
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.verify
import java.time.Duration
import java.time.LocalDateTime
import java.util.UUID
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import models.Player
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Nested

class SimplePlayerTimeTest {

    private var dbDir = createTempDir()

    @BeforeTest
    fun setUp() {
        dbDir = createTempDir()
    }

    @AfterTest
    fun cleanUp() {
        dbDir.delete()
    }

    @Nested
    inner class AddPlayerTest {
        @Test
        fun `should add new player`() {
            val uuid = UUID.randomUUID()

            val mockPlayerTimeDB = mockk<PlayerTimeDB>()
            every { mockPlayerTimeDB.existsById(uuid) } returns false
            every { mockPlayerTimeDB.create(any()) } just Runs
            every { mockPlayerTimeDB.save(any()) } just Runs

            val playerService = SimplePlayerTime(mockPlayerTimeDB)

            playerService.playerJoin(uuid, "playerName")

            verify { mockPlayerTimeDB.save(any()) }
            verify { mockPlayerTimeDB.create(any()) }
        }

        @Test
        fun `should add existing player`() {
            val uuid = UUID.randomUUID()
            val player = Player(uuid, "playerName")

            val mockPlayerTimeDB = mockk<PlayerTimeDB>()
            every { mockPlayerTimeDB.existsById(uuid) } returns true
            every { mockPlayerTimeDB.findById(uuid) } returns player
            every { mockPlayerTimeDB.save(any()) } just Runs

            val playerService = SimplePlayerTime(mockPlayerTimeDB)

            playerService.playerJoin(uuid, "playerName")

            verify { mockPlayerTimeDB.save(any()) }
            verify { mockPlayerTimeDB.findById(uuid) }
            verify { mockPlayerTimeDB.existsById(uuid) }
        }

        @Test
        fun `should add existing player and update the username`() {
            val capturedPlayer = mutableListOf<Player>()
            val uuid = UUID.randomUUID()
            val player = Player(uuid, "playerName")
            val updateName = "updateName"

            val mockPlayerTimeDB = mockk<PlayerTimeDB>()
            every { mockPlayerTimeDB.existsById(uuid) } returns true
            every { mockPlayerTimeDB.findById(any()) } returns player
            every { mockPlayerTimeDB.save(capture(capturedPlayer)) } just Runs

            val playerService = SimplePlayerTime(mockPlayerTimeDB)

            playerService.playerJoin(uuid, updateName)

            assertEquals(1, capturedPlayer.size)
            assertEquals(updateName, capturedPlayer.first().playerName)
        }

        @Test
        fun `should change join time by existing player`() {
            val capturedPlayer = mutableListOf<Player>()
            val uuid = UUID.randomUUID()
            val player = Player(uuid, "playerName")
            player.joinTime = LocalDateTime.now().minusDays(1)
            val originalPlayer = player.copy()

            val mockPlayerTimeDB = mockk<PlayerTimeDB>()
            every { mockPlayerTimeDB.existsById(uuid) } returns true
            every { mockPlayerTimeDB.findById(uuid = any()) } returns player
            every { mockPlayerTimeDB.save(capture(capturedPlayer)) } just Runs

            val playerService = SimplePlayerTime(mockPlayerTimeDB)

            playerService.playerJoin(uuid, "playerName")

            assertEquals(1, capturedPlayer.size)
            assertNotEquals(originalPlayer.joinTime, capturedPlayer.first().joinTime)
        }
    }

    @Test
    fun `update the play time and save`() {
        val capturedPlayers = mutableListOf<Player>()
        val uuid = UUID.randomUUID()
        val player = Player(uuid, "", playTime = Duration.ZERO)

        val mockDBPlayerTimeDB = mockk<PlayerTimeDB>()
        every { mockDBPlayerTimeDB.findById(uuid) } returns player
        every { mockDBPlayerTimeDB.save(capture(capturedPlayers)) } just Runs

        val simplePlayerTime = SimplePlayerTime(mockDBPlayerTimeDB)

        simplePlayerTime.updatePlayTime(uuid)

        assertEquals(1, capturedPlayers.size)
        assertNotEquals(Duration.ZERO, capturedPlayers.first().playTime)
    }

    @Nested
    inner class TimePlayedTest {

        @Test
        fun `get played time and update the field`() {
            val uuid = UUID.randomUUID()
            val player = Player(uuid, "playerName")

            val mockPlayerTimeDB = mockk<PlayerTimeDB>()
            every { mockPlayerTimeDB.existsById(any()) } returns true
            every { mockPlayerTimeDB.findById(uuid) } returns player
            every { mockPlayerTimeDB.save(any()) } just Runs

            val playerService = SimplePlayerTime(mockPlayerTimeDB)

            playerService.playerJoin(uuid, "playerName")

            // assertFalse(playerService.isEmpty())

            val playTime = playerService.timePlayed(uuid)

            assertNotEquals(Duration.ZERO, playTime)
            assertFalse(playTime.isNegative)
            // assertEquals(Duration.ZERO, playerService.getPlayer(uuid).playTime)
        }

        @Test
        fun `get played time after the time was saved`() {
            val uuid = UUID.randomUUID()
            val player = Player(uuid, "playerName", lastSave = LocalDateTime.now().plusMinutes(10))

            val mockPlayerTimeDB = mockk<PlayerTimeDB>()
            every { mockPlayerTimeDB.existsById(any()) } returns true
            every { mockPlayerTimeDB.findById(uuid) } returns player
            every { mockPlayerTimeDB.save(any()) } just Runs

            val playerService = SimplePlayerTime(mockPlayerTimeDB)

            playerService.playerJoin(uuid, "playerName")

            val playTime = playerService.timePlayed(uuid)

            assertTrue(
                playTime.toMinutes() <= -9,
                "the result is negative because it is a synthetic test. The playtime is " + playTime.toMinutes()
            )
        }

        @Test
        fun `get played time from the join when the save time earlier than the join time`() {
            val uuid = UUID.randomUUID()
            val mockPlayer = Player(uuid, "playerName", lastSave = LocalDateTime.now().minusMinutes(10))

            val mockPlayerTimeDB = mockk<PlayerTimeDB>()
            every { mockPlayerTimeDB.existsById(any()) } returns true
            every { mockPlayerTimeDB.findById(uuid) } returns mockPlayer
            every { mockPlayerTimeDB.save(any()) } just Runs

            val playerService = SimplePlayerTime(mockPlayerTimeDB)

            playerService.playerJoin(uuid, "playerName")

            // assertFalse(playerService.isEmpty())

            val playTime = playerService.timePlayed(uuid)

            assertNotEquals(Duration.ZERO, playTime)
            assertFalse(playTime.isNegative)
            println(playTime.toMinutes())
        }
    }

    @Nested
    inner class TopPlayer {

        @Test
        fun `get list with players`() {
            val player1 = Player(UUID.randomUUID(), "player1", playTime = Duration.ZERO)
            val player2 = Player(UUID.randomUUID(), "player2", playTime = Duration.ofMinutes(10))

            val mockPlayerTimeDB = mockk<PlayerTimeDB>()
            every { mockPlayerTimeDB.findAll() } returns listOf(player1, player2)

            val playerService = SimplePlayerTime(mockPlayerTimeDB)

            val topPlayer = playerService.getTopPlayers()

            assertFalse(topPlayer.isEmpty())
            assertEquals(2, topPlayer.size)
        }

        @Test
        fun `get list with players sorted by most time`() {
            val player1 = Player(UUID.randomUUID(), "player1", playTime = Duration.ZERO)
            val player2 = Player(UUID.randomUUID(), "player2", playTime = Duration.ofMinutes(10))
            val player3 = Player(UUID.randomUUID(), "player3", playTime = Duration.ofHours(2))

            val mockPlayerTimeDB = mockk<PlayerTimeDB>()
            every { mockPlayerTimeDB.findAll() } returns listOf(player1, player2, player3)

            val playerService = SimplePlayerTime(mockPlayerTimeDB)

            val topPlayer = playerService.getTopPlayers()

            assertEquals(player3, topPlayer[0])
            assertEquals(player2, topPlayer[1])
            assertEquals(player1, topPlayer[2])
        }
    }
}
