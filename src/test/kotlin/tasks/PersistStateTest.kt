package tasks

import SimplePlayerTime
import io.mockk.Runs
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.unmockkAll
import io.mockk.verify
import java.util.UUID
import kotlin.test.AfterTest
import kotlin.test.Test
import kotlin.test.assertEquals
import org.bukkit.Bukkit
import org.bukkit.entity.Player

class PersistStateTest {

    @AfterTest
    fun cleanUp() {
        unmockkAll()
    }

    @Test
    fun `should persist all players`() {
        val captureList = mutableListOf<UUID>()
        val uuids = listOf<UUID>(UUID.randomUUID(), UUID.randomUUID())

        val player1 = mockk<Player>()
        val player2 = mockk<Player>()
        every { player1.uniqueId } returns uuids[0]
        every { player2.uniqueId } returns uuids[1]

        mockkStatic(Bukkit::class)
        every { Bukkit.getOnlinePlayers() } returns listOf(player1, player2)

        val mockPlayerService = mockk<SimplePlayerTime>()
        every { mockPlayerService.updatePlayTime(capture(captureList)) } just Runs

        val persistState = PersistState(mockPlayerService)
        persistState.run()

        assertEquals(2, captureList.size)

        verify(exactly = 2) { mockPlayerService.updatePlayTime(any()) }
        confirmVerified(mockPlayerService)
    }
}
