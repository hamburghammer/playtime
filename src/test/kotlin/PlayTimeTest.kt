import java.time.Duration
import kotlin.test.Test
import kotlin.test.assertEquals

class PlayTimeTest {

    @Test
    fun `right day formatting`() {
        assertEquals("02:00:00", FakePlayerTime().timeToString(Duration.ofDays(2)))
        assertEquals("20:00:00", FakePlayerTime().timeToString(Duration.ofDays(20)))
    }

    @Test
    fun `right hour formatting`() {
        assertEquals("00:02:00", FakePlayerTime().timeToString(Duration.ofHours(2)))
        assertEquals("00:20:00", FakePlayerTime().timeToString(Duration.ofHours(20)))
    }

    @Test
    fun `right minutes formatting`() {
        assertEquals("00:00:02", FakePlayerTime().timeToString(Duration.ofMinutes(2)))
        assertEquals("00:00:20", FakePlayerTime().timeToString(Duration.ofMinutes(20)))
    }
}
