package commands

import java.time.LocalDateTime
import kotlin.test.Test
import org.junit.jupiter.api.Assertions.assertEquals

internal class UpTimeTest {

    @Test
    fun `should set current time`() {
        val now = LocalDateTime.now()
        val upTime = UpTime(now)
        assertEquals(now, upTime.startTime)
    }

    @Test
    fun `should calculate time passed`() {
        val startTime = LocalDateTime.of(2019, 12, 27, 15, 33)
        val upTime = UpTime(startTime)

        assertEquals(listOf(0, 0, 35L), upTime.timePassed(startTime.plusMinutes(35)))
        assertEquals(listOf(0, 1L, 5L), upTime.timePassed(startTime.plusMinutes(65)))
        assertEquals(listOf(0, 2L, 0), upTime.timePassed(startTime.plusHours(2)))
        assertEquals(listOf(1L, 1L, 0), upTime.timePassed(startTime.plusHours(25)))
    }
}
