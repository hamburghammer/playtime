package commands

import FakePlayerTime
import SimplePlayerTime
import db.PlayerNotFoundException
import db.PlayerTimeDB
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.unmockkAll
import java.time.Duration
import java.util.UUID
import kotlin.test.AfterTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import models.Player
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.junit.jupiter.api.Assertions.assertFalse

internal class PlayTimeOfCommandTest {

    @AfterTest
    fun cleanUp() {
        unmockkAll()
    }

    @Test
    fun `the there is no arg`() {
        val mockPlayerService = mockk<SimplePlayerTime>()
        val mockPlayerTimeDB = mockk<PlayerTimeDB>()

        val playTimeOfCommand = PlayTimeOfCommand(mockPlayerService, mockPlayerTimeDB)

        assertFalse(playTimeOfCommand.onCommand(mockk(), mockk(), "", arrayOf()))
    }

    @Test
    fun `the player can not be read`() {
        val nonExistingPlayerName = "foo"
        val captureList = mutableListOf<String>()
        val mockPlayerService = mockk<SimplePlayerTime>()
        val mockPlayerTimeDB = mockk<PlayerTimeDB>()
        every { mockPlayerTimeDB.findByName(nonExistingPlayerName) } throws (PlayerNotFoundException(""))

        val playTimeOfCommand = PlayTimeOfCommand(mockPlayerService, mockPlayerTimeDB)

        val mockCommandSender = mockk<CommandSender>()
        every { mockCommandSender.sendMessage(capture(captureList)) } just Runs

        assertFalse(playTimeOfCommand.onCommand(mockCommandSender, mockk(), "", arrayOf(nonExistingPlayerName)))
        assertEquals(1, captureList.size)
        assertEquals("""There is no player with the name $nonExistingPlayerName""", captureList.first())
    }

    @Test
    fun `player found online`() {
        val captureList = mutableListOf<String>()
        val playerName = "foo"
        val uuid = UUID.randomUUID()
        val player = Player(uuid, playerName)
        val duration = Duration.ZERO
        val timeToString = FakePlayerTime().timeToString(duration)

        val mockPlayerService = mockk<SimplePlayerTime>()
        every { mockPlayerService.timePlayed(uuid) } returns duration
        every { mockPlayerService.timeToString(duration) } returns timeToString

        val mockPlayerTimeDB = mockk<PlayerTimeDB>()
        every { mockPlayerTimeDB.findByName(playerName) } returns player

        val playTimeOfCommand = PlayTimeOfCommand(mockPlayerService, mockPlayerTimeDB)

        val mockCommandSender = mockk<CommandSender>()
        every { mockCommandSender.sendMessage(capture(captureList)) } just Runs

        mockkStatic(Bukkit::class)
        every { Bukkit.getPlayer(uuid)!!.isOnline } returns true

        assertTrue(playTimeOfCommand.onCommand(mockCommandSender, mockk(), "", arrayOf(playerName)))
        assertEquals(1, captureList.size)
        assertEquals("""The play time from $playerName is $timeToString""", captureList.first())
    }

    @Test
    fun `player found not online`() {
        val captureList = mutableListOf<String>()
        val playerName = "foo"
        val uuid = UUID.randomUUID()
        val player = Player(uuid, playerName)
        val duration = Duration.ZERO
        val timeToString = FakePlayerTime().timeToString(duration)

        val mockPlayerService = mockk<SimplePlayerTime>()
        every { mockPlayerService.timeToString(duration) } returns timeToString

        val mockPlayerTimeDB = mockk<PlayerTimeDB>()
        every { mockPlayerTimeDB.findByName(playerName) } returns player

        val playTimeOfCommand = PlayTimeOfCommand(mockPlayerService, mockPlayerTimeDB)

        val mockCommandSender = mockk<CommandSender>()
        every { mockCommandSender.sendMessage(capture(captureList)) } just Runs

        mockkStatic(Bukkit::class)
        every { Bukkit.getPlayer(uuid)!!.isOnline } returns false

        assertTrue(playTimeOfCommand.onCommand(mockCommandSender, mockk(), "", arrayOf(playerName)))
        assertEquals(1, captureList.size)
        assertEquals("""The play time from $playerName is $timeToString""", captureList.first())
    }
}
