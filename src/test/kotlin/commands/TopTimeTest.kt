package commands

import SimplePlayerTime
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.unmockkAll
import java.time.Duration
import java.util.UUID
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import models.Player
import org.bukkit.command.CommandSender
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Assertions.assertTrue

class TopTimeTest {

    @BeforeTest
    fun setUp() {
        unmockkAll()
    }

    @Test
    fun `send string`() {
        val captureList = mutableListOf<String>()

        val uuid1 = UUID.randomUUID()
        val uuid2 = UUID.randomUUID()
        val uuid3 = UUID.randomUUID()
        val player1 = Player(uuid1, "player1", playTime = Duration.ZERO)
        val player2 = Player(uuid2, "player2", playTime = Duration.ofMinutes(10))
        val player3 = Player(uuid3, "player3", playTime = Duration.ofHours(2))

        val mockPlayTime = mockk<SimplePlayerTime>()
        every { mockPlayTime.getTopPlayers() } returns listOf(player3, player2, player1)
        every { mockPlayTime.timeToString(any()) } returns "00:00:00"

        val topTime = TopTime(mockPlayTime)

        val mockCommandSender = mockk<CommandSender>()
        every { mockCommandSender.sendMessage(capture(captureList)) } just Runs

        assertTrue(topTime.onCommand(mockCommandSender, mockk(), "", arrayOf()))

        assertTrue(captureList.isNotEmpty())
        assertNotEquals("", captureList.first())
    }

    @Test
    fun `correct formatted string output`() {
        val captureList = mutableListOf<String>()

        val uuid1 = UUID.randomUUID()
        val uuid2 = UUID.randomUUID()
        val uuid3 = UUID.randomUUID()
        val player1 = Player(uuid1, "player1", playTime = Duration.ZERO)
        val player2 = Player(uuid2, "player2", playTime = Duration.ofMinutes(10))
        val player3 = Player(uuid3, "player3", playTime = Duration.ofHours(2))

        val mockPlayTime = mockk<SimplePlayerTime>()
        every { mockPlayTime.getTopPlayers() } returns listOf(player3, player2, player1)
        every { mockPlayTime.timeToString(any()) } returns "00:00:00"

        val mockCommandSender = mockk<CommandSender>()
        every { mockCommandSender.sendMessage(capture(captureList)) } just Runs

        val topTime = TopTime(mockPlayTime)
        assertTrue(topTime.onCommand(mockCommandSender, mockk(), "", arrayOf()))

        assertEquals(
            """Top list
player3: 00:00:00
player2: 00:00:00
player1: 00:00:00
""", captureList.first().toString()
        )
    }

    @Test
    fun `show only top 6 player`() {
        val playerListSize = 6
        val defaultNewLines = 2
        val captureList = mutableListOf<String>()

        val playerList = mutableListOf<Player>()
        for (i in 1..7) {
            playerList.add(Player(UUID.randomUUID(), "$i", playTime = Duration.ZERO))
        }

        val mockPlayTime = mockk<SimplePlayerTime>()
        every { mockPlayTime.getTopPlayers() } returns playerList.toList()
        every { mockPlayTime.timeToString(any()) } returns "00:00:00"

        val mockCommandSender = mockk<CommandSender>()
        every { mockCommandSender.sendMessage(capture(captureList)) } just Runs

        val topTime = TopTime(mockPlayTime, playerListSize.toLong())
        assertTrue(topTime.onCommand(mockCommandSender, mockk(), "", arrayOf()))

        assertEquals(playerListSize + defaultNewLines, captureList.first().split("\n").count())
    }
}
