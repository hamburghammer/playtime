package listener

import PlayerTime
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.verify
import java.util.UUID
import org.bukkit.entity.Player
import org.bukkit.event.player.PlayerQuitEvent
import org.junit.jupiter.api.Test

class QuitListenerTest {

    @Test
    fun `should call to add a player`() {
        val uuid = UUID.randomUUID()
        val mockPlayer = mockk<Player>()
        every { mockPlayer.uniqueId } returns uuid
        every { mockPlayer.name } returns ""

        val mockPlayerQuitEvent = mockk<PlayerQuitEvent>()
        every { mockPlayerQuitEvent.player } returns mockPlayer

        val mockPlayerDB = mockk<PlayerTime>()
        every { mockPlayerDB.updatePlayTime(any()) } just Runs

        val quitListener = QuitListener(mockPlayerDB)
        quitListener.onPlayerQuit(mockPlayerQuitEvent)

        verify { mockPlayerDB.updatePlayTime(any()) }
    }
}
