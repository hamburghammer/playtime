package listener

import PlayerTime
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.verify
import java.util.UUID
import org.bukkit.entity.Player
import org.bukkit.event.player.PlayerJoinEvent
import org.junit.jupiter.api.Test

class JoinListenerTest {

    @Test
    fun `should call to add a player`() {
        val uuid = UUID.randomUUID()

        val mockPlayer = mockk<Player>()
        every { mockPlayer.uniqueId } returns uuid
        every { mockPlayer.name } returns ""

        val mockPlayerJoinEvent = mockk<PlayerJoinEvent>()
        every { mockPlayerJoinEvent.player } returns mockPlayer

        val mockPlayerDB = mockk<PlayerTime>()
        every { mockPlayerDB.playerJoin(any(), any()) } just Runs

        val joinListener = JoinListener(mockPlayerDB)
        joinListener.onPlayerJoin(mockPlayerJoinEvent)

        verify { mockPlayerDB.playerJoin(any(), any()) }
    }
}
