import db.PlayerTimeDB
import java.time.Duration
import java.time.LocalDateTime
import java.util.UUID
import models.Player

/**
 * A service to hold the all player that are currently playing
 * It should only be used as Singleton @see Kotlin objects
 *
 * @property db the DB implementation which should be used to persist the state
 */
class SimplePlayerTime(private val db: PlayerTimeDB) : PlayerTime {

    /**
     * Adds the player to the [db].
     * It creates an new player if it's no persisted and adds/refreshes the join time
     *
     * @param uuid the actual uuid of a player
     * @param playerName for updating or to create a new player
     */
    override fun playerJoin(uuid: UUID, playerName: String) {
        val player: Player = if (db.existsById(uuid)) {
            val dbPlayer = db.findById(uuid)
            dbPlayer.joinTime = LocalDateTime.now()
            dbPlayer.playerName = playerName
            dbPlayer
        } else {
            val player = Player(uuid, playerName)
            db.create(player)
            player
        }
        db.save(player)
    }

    /**
     * Updates the playtime and saves it from the player with the [uuid]
     *
     * @param uuid UUID
     */
    override fun updatePlayTime(uuid: UUID) {
        val player = db.findById(uuid)
        player.playTime = calculatePlaytime(player)
        db.save(player)
    }

    /**
     * Returns the playtime of the player with the the [uuid]
     *
     * @param uuid player uuid
     *
     * @return the total playtime
     */
    override fun timePlayed(uuid: UUID): Duration {
        val player = db.findById(uuid)
        return calculatePlaytime(player)
    }

    /**
     * Search for the players with the highest playtime
     *
     * @return List<Player> in a descending order by playtime
     */
    override fun getTopPlayers(): List<Player> {
        val playerList: List<Player> = db.findAll().onEach { player -> player.playTime = calculatePlaytime(player) }
        return playerList.sortedByDescending { player -> player.playTime }
    }

    /**
     * Calculates the time based on the join and on the last save time of the player
     * if the playtime got saved it should use the saved time instead of the join time
     *
     * @param player Player
     * @return Duration the playtime of the [player]
     */
    private fun calculatePlaytime(player: Player): Duration {
        val playTime: Duration =
            if ((player.lastSave != null) && Duration.between(player.lastSave, player.joinTime).isNegative) {
                Duration.between(player.lastSave, LocalDateTime.now())
            } else {
                Duration.between(player.joinTime, LocalDateTime.now())
            }
        return Duration.from(player.playTime).plus(playTime)
    }
}
