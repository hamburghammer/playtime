package tasks

import PlayerTime
import PlayerTimeService
import org.bukkit.Bukkit

class PersistState(private val playerTime: PlayerTime = PlayerTimeService) : Runnable {

    override fun run() {
        Bukkit.getOnlinePlayers().forEach { player -> playerTime.updatePlayTime(player.uniqueId) }
    }
}
