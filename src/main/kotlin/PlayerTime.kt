import java.time.Duration
import java.util.UUID
import models.Player

interface PlayerTime {
    /**
     * Adds the player to the playerMap.
     * It creates an new player if it's no persisted and adds/refreshes the join time
     *
     * @param uuid the actual uuid of a player
     * @param playerName for updating or to create a new player
     */
    fun playerJoin(uuid: UUID, playerName: String)

    /**
     * Updates the playtime and saves it
     *
     * @param uuid UUID
     */
    fun updatePlayTime(uuid: UUID)

    /**
     * Returns the playtime including the old time
     * if the stats got save it should use the save time instead of the join time
     *
     * @param uuid player uuid
     *
     * @return the total playtime
     */
    fun timePlayed(uuid: UUID): Duration

    /**
     * Search for the players with the highest playtime
     *
     * @return List<Player>
     */
    fun getTopPlayers(): List<Player>

    /**
     * Formats the duration to a simple String
     *
     * @param duration Duration
     * @return String (DD:HH:MM)
     */
    fun timeToString(duration: Duration): String {
        val days: String =
            if (duration.toDaysPart() < 10) """0${duration.toDaysPart()}""" else """${duration.toDaysPart()}"""
        val hours: String =
            if (duration.toHoursPart() < 10) """0${duration.toHoursPart()}""" else """${duration.toHoursPart()}"""
        val minutes: String =
            if (duration.toMinutesPart() < 10) """0${duration.toMinutesPart()}""" else """${duration.toMinutesPart()}"""
        return """$days:$hours:$minutes"""
    }
}
