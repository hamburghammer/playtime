package db

import java.lang.Exception

class PlayerNotFoundException(message: String) : Exception(message)
