package db

import java.util.UUID
import models.Player

interface PlayerFinder {

    /**
     * It should look if a player exists
     *
     * @param uuid the id of the player
     *
     * @return if the player is present
     */
    fun existsById(uuid: UUID): Boolean

    /**
     * It should look if a player exists
     * If uuid of the player available use @see db.PlayerTimeDB.findByID due to the option to change the username
     *
     * @param name the name of the player
     *
     * @return if the player is present
     */
    fun existsByName(name: String): Boolean

    /**
     * Should read the player
     *
     * @param uuid the uuid of the player that should be read
     *
     * @return the actual player
     *
     * @throws PlayerNotFoundException
     */
    fun findById(uuid: UUID): Player

    /**
     * Should read the player
     *
     * @param name the name of the player that should be read
     *
     * @return the actual player
     *
     * @throws PlayerNotFoundException
     */
    fun findByName(name: String): Player

    /**
     * Should read all player and return theme
     *
     * @return List<Player>
     */
    fun findAll(): List<Player>
}
