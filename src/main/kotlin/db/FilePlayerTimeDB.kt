package db

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import java.io.File
import java.time.LocalDateTime
import java.util.UUID
import models.Player
import mu.KotlinLogging

/**
 * FileDB manages the file based PlayerTime DB and provides basic functionality to interact with it
 *
 * @property path optional path for the storage of the data
 * @property gson gson object to work with json
 */
class FilePlayerTimeDB(private val path: File = File("./plugins/PlayTime/")) : PlayerTimeDB {

    val logger = KotlinLogging.logger {}

    private val gson = Gson()

    /**
     * Ensures that the directory exists
     */
    init {
        path.mkdirs()
    }

    /**
     * It looks if a certain file already exits
     *
     * @param uuid the id for the file that should be found
     *
     * @return if the file is present
     */
    override fun existsById(uuid: UUID): Boolean {
        return File(path, uuid.toString()).exists()
    }

    /**
     * It should look if a player exists by iterating over all saved players
     *
     * @param name the id of the player
     *
     * @return if the player is present
     */
    override fun existsByName(name: String): Boolean {
        val fileList = path.listFiles() ?: return false
        if (fileList.isEmpty()) return false
        for (file in fileList) {
            val player = gson.fromJson<Player>(file.readText(), Player::class.java)
            if (player.playerName == name) {
                return true
            }
        }
        return false
    }

    /**
     * Creates a player file
     *
     * @param player the player that should be saved to the new file
     *
     */
    override fun create(player: Player) {
        val file = File(path, player.uuid.toString())
        if (!file.exists()) file.createNewFile()
        save(player)
    }

    /**
     * Reads the player out of a file
     *
     * @param uuid the id of the player that should be read
     *
     * @return Player
     *
     * @throws PlayerNotFoundException
     */
    override fun findById(uuid: UUID): Player {
        val file = File(path, uuid.toString())
        if (!file.exists()) throw PlayerNotFoundException("""The player with the id $uuid was not found""")
        return gson.fromJson(file.readText(), Player::class.java)
    }

    /**
     * Reads the player out of a file by finding the right one
     *
     * @param name the name of the player that should be read
     *
     * @return Player? if present
     */
    override fun findByName(name: String): Player {
        val fileList =
            path.listFiles() ?: throw PlayerNotFoundException("""The player with the name $name was not found""")
        if (fileList.isEmpty()) throw PlayerNotFoundException("""The player with the name $name was not found""")
        for (file in fileList) {
            val player = gson.fromJson<Player>(file.readText(), Player::class.java)
            if (player.playerName == name) {
                return player
            }
        }
        throw PlayerNotFoundException("""The player with the name $name was not found""")
    }

    /**
     * Should read all player and return theme
     *
     * @return List<Player>
     */
    override fun findAll(): List<Player> {
        val playerList = mutableListOf<Player>()
        val fileList = path.listFiles() ?: return emptyList()
        fileList.forEach { file: File ->
            try {
                playerList.add(gson.fromJson(file.readText(), Player::class.java))
            } catch (e: JsonSyntaxException) {
                logger.error { "The player with the ID ${{ file.name }} could not be read out of the files!" }
            }
        }
        return playerList.toList()
    }

    /**
     * Writs a player into an existing file
     * To create a new player file @see createPlayer(player: Player)
     *
     * @param player that should be written
     */
    override fun save(player: Player) {
        val file = File(path, player.uuid.toString())
        player.lastSave = LocalDateTime.now()
        file.writeText(gson.toJson(player))
    }
}
