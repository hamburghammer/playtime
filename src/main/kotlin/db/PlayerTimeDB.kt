package db

import models.Player

interface PlayerTimeDB : PlayerFinder {

    /**
     * Should create a player record for persisting
     *
     * @param player the player that should be save
     */
    fun create(player: Player)

    /**
     * Should write a player into persistence
     * To create a new player file @see db.PlayerTimeDB.createPlayer
     *
     * @param player that should be written
     */
    fun save(player: Player)
}
