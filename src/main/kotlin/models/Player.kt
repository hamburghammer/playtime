package models

import java.time.Duration
import java.time.LocalDateTime
import java.util.UUID

/**
 * The representation of a in game player for the online time
 *
 * @property uuid the unique identify of the player given by Mojang
 * @property playerName the username of the player which is not that important due to the fact that a player can change it. It should be used only for debugging purposes
 * @property playTime the actual playtime. The default value for initialization is zero
 * @property joinTime the last time when the player join the server#
 * @property lastSave should hold the time when it was last saved if it wasn't it should be null
 */
data class Player(
    val uuid: UUID,
    var playerName: String,
    var playTime: Duration = Duration.ZERO,
    var joinTime: LocalDateTime = LocalDateTime.now(),
    var lastSave: LocalDateTime? = null
)
