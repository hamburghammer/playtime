package commands

import mu.KotlinLogging
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

/**
 * Playground for testing
 * Pls ignore
 */
class TestCommand : CommandExecutor {

    private val log = KotlinLogging.logger { }

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        return if (sender is Player) {
            sender.sendMessage("u are a Player")
            true
        } else {
            sender.sendMessage("not Player")
            true
        }
    }
}
