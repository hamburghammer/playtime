package commands

import java.time.LocalDateTime
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender

/**
 * This is a command executor to print time the server is up and running.
 *
 * @property startTime the time the server starts. The default value is the object initialization but it's possible to change for example for testing
 */
class UpTime(val startTime: LocalDateTime = LocalDateTime.now()) : CommandExecutor {

    /**
     * It sends the uptime of the server to the CommandSender
     *
     * @param sender to send a message
     *
     * @return always true
     */
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {

        val timeList = timePassed(LocalDateTime.now())
        val days: String = if (timeList[0] < 10) """0${timeList[0]}""" else """${timeList[0]}"""
        val hours: String = if (timeList[1] < 10) """0${timeList[1]}""" else """${timeList[1]}"""
        val minutes: String = if (timeList[2] < 10) """0${timeList[2]}""" else """${timeList[2]}"""
        sender.sendMessage("""$days:$hours:$minutes""")
        return true
    }

    /**
     * Calculates the past time
     * If the uptime is longer than one year these 365 days will be ignored
     * @param now the time that will be used to calculate the time diff
     * @return a list with the time format DD,HH,MM
     */
    fun timePassed(now: LocalDateTime): List<Long> {

        val duration = java.time.Duration.between(startTime, now)

        return listOf(duration.toDaysPart(), duration.toHoursPart().toLong(), duration.toMinutesPart().toLong())
    }
}
