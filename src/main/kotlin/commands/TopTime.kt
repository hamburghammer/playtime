package commands

import PlayerTime
import PlayerTimeService
import java.util.stream.Collectors
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender

/**
 * Command to get the top players with the most playtime
 *
 * @property playerTime PlayerTime
 * @property playerListSize Long default = 5
 * @constructor
 */
class TopTime(private val playerTime: PlayerTime = PlayerTimeService, private val playerListSize: Long = 5) : CommandExecutor {

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        val playerTopList = playerTime.getTopPlayers().stream().limit(playerListSize).collect(Collectors.toList()).toList()

        val sb = StringBuilder().appendln("Top list")
        playerTopList.forEach { sb.appendln("${it.playerName}: ${playerTime.timeToString(it.playTime)}") }
        sender.sendMessage(sb.toString())

        return true
    }
}
