package commands

import PlayerTime
import PlayerTimeService
import db.FilePlayerTimeDB
import db.PlayerFinder
import db.PlayerNotFoundException
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender

class PlayTimeOfCommand(private val playerTime: PlayerTime = PlayerTimeService, private val playerTimeDB: PlayerFinder = FilePlayerTimeDB()) :
    CommandExecutor {

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<String>): Boolean {
        if (args.size != 1) {
            return false
        } else {
            val playerName = args.first()
            val player = try {
                playerTimeDB.findByName(playerName)
            } catch (e: PlayerNotFoundException) {
                sender.sendMessage("""There is no player with the name $playerName""")
                return false
            }

            val playerIsOnline = Bukkit.getPlayer(player.uuid)?.isOnline ?: false
            val time = if (playerIsOnline) {
                playerTime.timeToString(playerTime.timePlayed(player.uuid))
            } else {
                playerTime.timeToString(player.playTime)
            }

            sender.sendMessage("""The play time from ${player.playerName} is $time""")
            return true
        }
    }
}
