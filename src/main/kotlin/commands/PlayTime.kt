package commands

import PlayerTime
import PlayerTimeService
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

/**
 * The command executor for the playtime
 *
 * @property playerTime the service which holds the current status
 */
class PlayTime(private val playerTime: PlayerTime = PlayerTimeService) : CommandExecutor {

    /**
     * It sends the playtime of and to the CommandSender
     *
     * @param sender to send a message
     *
     * @return true when the sender is a actual player
     */
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {

        return if (sender !is Player) {
            sender.sendMessage("This command can only be executed as a player")
            false
        } else {
            val duration = playerTime.timePlayed(sender.uniqueId)

            val playTime = playerTime.timeToString(duration)

            sender.sendMessage("""Your play time: $playTime""")
            true
        }
    }
}
