#!/usr/bin/env bash

VERSION=0.2.0

sh ./gradlew shadowJar

cp build/libs/PlayTime-${VERSION}-SNAPSHOT-all.jar minecraftTestServer/plugins/

rm minecraftTestServer/plugins/PlayTime/*

cd minecraftTestServer/

java -Xmx2G -jar spigot-1.15.1.jar